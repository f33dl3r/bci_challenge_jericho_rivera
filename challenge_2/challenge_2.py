"""
Difficulty: medium
"""

# import required libraries
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager

# launch url
url = 'https://businesspapers.parracity.nsw.gov.au/'

# set browser and options
options = Options()
options.add_experimental_option('excludeSwitches', ['enable-logging'])

driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
driver.implicitly_wait(30)
driver.get(url)

content = []

# get html source code
soup = BeautifulSoup(driver.page_source, 'html5lib')

table = soup.find('table', attrs={'id': 'grdMenu'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')

    # set variables for output
    date = cols[0].text.strip()
    meeting_text = cols[1].text.strip()
    agenda_htm = " "
    agenda_pdf = " "
    meeting_htm = " "

    # remove unnecessary text from the output
    span = cols[1].find('span').text.strip()
    meeting = meeting_text.replace(span, "")

    # check if agendas column is null
    if cols[2]:
        if cols[2].find('a', attrs={'class': 'bpsGridHTMLLink'}):
            agenda_htm = url + cols[2].find(
                'a', attrs={'class': 'bpsGridHTMLLink'})['href']

        if cols[2].find('a', attrs={'class': 'bpsGridPDFLink'}):
            agenda_pdf = url + cols[2].find(
                'a', attrs={'class': 'bpsGridPDFLink'})['href']

    # check if minutes column is null
    if cols[4]:
        if cols[4].find('a', attrs={'class': 'bpsGridHTMLLink'}):
            meeting_htm = url + cols[4].find(
                'a', attrs={'class': 'bpsGridHTMLLink'})['href']

    # temporary array to consolidate variables
    arr = [date, meeting, agenda_htm, agenda_pdf, meeting_htm]

    # add to content array for csv
    content.append(arr)

# output to csv
df = pd.DataFrame(content)
df.columns = ["meeting_date", "meeting_description",
              "agenda_html_link", "agenda_pdf_link",	"minutes"]
df.to_csv('challenge_2_output.csv', index=False)

driver.quit()  # close browser
