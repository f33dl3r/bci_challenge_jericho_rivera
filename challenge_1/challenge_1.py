"""
Difficulty: easy
"""
# import required libraries
from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

# declare url
url = 'https://www.emservices.com.sg/tenders/'

# set options for browser
options = Options()
options.add_experimental_option('excludeSwitches', ['enable-logging'])

# remove this option if you only need the first page
options.add_experimental_option(
    "prefs", {'profile.managed_default_content_settings.javascript': 2})

# download chrome driver for selenium
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
driver.implicitly_wait(30)
driver.get(url)

# empty array for table contents
content = []

# get html source code of the url
soup = BeautifulSoup(driver.page_source, 'html5lib')

# get table from the html source code
table = soup.find('table', attrs={'id': 'tnTable1'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')

# loop through the table to get the data
for row in rows:
    cols = row.find_all('td')
    arr = [cols[0].text.strip(), cols[1].text.strip(), cols[2].text.strip(
    ), cols[3].text.strip(), cols[4].text.strip(), cols[3].a['href']]

    content.append(arr)

# output as CSV
df = pd.DataFrame(content)
df.columns = ["advert_date", "closing_dates",
              "client", "description", "eligibility", "link"]
df.to_csv('challenge_1_output.csv', index=False)

driver.quit()  # close browser
